SysProg Projektarbeit Template
==============================

Voraussetzungen
---------------

- Java (für PlantUML)

Ausführung
----------

Um PlantUML nutzen zu können, muss pdflatex mit ```-shell-escape``` ausgeführt werden.

compile.sh
----------

Das Projekt kann über die ``compile.sh`` compiliert werden.

````shell
./compile.sh report
````
